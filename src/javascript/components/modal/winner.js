import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const winnerInfo = {
    title: 'WINNER IS!',
    bodyElement: fighter.name,
  };

  showModal(winnerInfo);
}
